import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/app_theme.dart';
import 'package:trill/asset_button.dart';
import 'package:trill/global.dart' as global;
import 'package:trill/kyc_alert.dart';
import 'package:trill/main_screen.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        KYCAlert(),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 18.0),
            child: Text(
              AppString.string(AppStringEnum.bank),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(
            context,
            FinancialAsset('US Dollar',
                type: FinancialAssetType.bank,
                image: Image.asset(
                  "assets/usd.png",
                  color: Colors.black,
                ))),
        buildAssetButton(
            context,
            FinancialAsset('Euro',
                type: FinancialAssetType.bank,
                image: Image.asset(
                  "assets/eur.png",
                  color: Colors.black,
                ))),
        buildAssetButton(
            context,
            FinancialAsset('Pound sterling',
                type: FinancialAssetType.bank,
                image: Image.asset(
                  "assets/gbp.png",
                  color: Colors.black,
                ))),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 18.0),
            child: Text(
              AppString.string(AppStringEnum.crypto),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(
            context,
            FinancialAsset('BTC',
                type: FinancialAssetType.crypto,
                image: Image.asset(
                  "assets/btc.png",
                  color: Colors.black,
                ))),
        buildAssetButton(
            context,
            FinancialAsset('ETH',
                type: FinancialAssetType.crypto,
                image: Image.asset(
                  "assets/eth.png",
                  color: Colors.black,
                ))),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 18.0),
            child: Text(
              AppString.string(AppStringEnum.trading),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(context, FinancialAsset('GAZP')),
        buildAssetButton(context, FinancialAsset('SBER')),
      ],
    );
  }
}

