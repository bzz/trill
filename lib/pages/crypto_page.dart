import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/asset_button.dart';
import 'package:trill/global.dart' as global;
import 'package:trill/kyc_alert.dart';
import 'package:trill/main_screen.dart';
import 'package:trill/global.dart' as global;

class CryptoPage extends StatefulWidget {
  @override
  _CryptoPageState createState() => _CryptoPageState();
}

class _CryptoPageState extends State<CryptoPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        KYCAlert(),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left:18.0),
            child: Text(
              AppString.string(AppStringEnum.crypto),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(context, FinancialAsset('BTC', type:FinancialAssetType.crypto, image: Image.asset(
          "assets/btc.png",
          color: Colors.black,
        ))),
        buildAssetButton(context, FinancialAsset('ETH', type:FinancialAssetType.crypto, image: Image.asset(
          "assets/eth.png",
          color: Colors.black,
        ))),
      ],
    );
  }
}
