import 'dart:io';
import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/global.dart' as global;
import 'app_theme.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppTheme.colors.backgroundBlue,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            elevation: 0,
            pinned: true,
            snap: true,
            floating: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text(AppString.string(AppStringEnum.profile)),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                switch (index) {
                  case 0:
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Padding(
                          padding: EdgeInsets.only(top: 30),
                          child: Image.asset('assets/person.png'),
                        ),
                        Padding(
                          padding: EdgeInsets.all(16),
                          child: Text('Name', style: Theme.of(context).textTheme.headline6),
                        ),
                      ],
                    );
                  case 1:
                    return Padding(
                      padding: EdgeInsets.only(left: 16),
                      child: Text('Info', style: Theme.of(context).textTheme.headline6),
                    );
                  case 2:
                    return Padding(
                      padding: EdgeInsets.all(16),
                      child: Container(
                        decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(24)),
                        child: Padding(
                          padding: EdgeInsets.all(16),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Name', style: Theme.of(context).textTheme.overline),
                              Text('Name Surname'),
                              Divider(
                                height: 34,
                              ),
                              Text('Phone', style: Theme.of(context).textTheme.overline),
                              Text(global.prefs.phone),
                              Divider(
                                height: 34,
                              ),
                              Text('Address', style: Theme.of(context).textTheme.overline),
                              Text('City name'),
                              Divider(
                                height: 34,
                              ),
                              Text('Password', style: Theme.of(context).textTheme.overline),
                              Text('\u2022\u2022\u2022\u2022\u2022\u2022\u2022\u2022'),
                              Divider(
                                height: 34,
                              ),
                              GestureDetector(
                                onTap: () {
                                  global.prefs.loggedIn = false;
                                  Navigator.pushNamed(context, '/login');
                                },
                                child: Row(
                                  children: [
                                    ImageIcon(
                                      AssetImage('assets/logout.png'),
                                      size: 24,
                                      color: Colors.red,
                                    ),
//                                  Image.asset('assets/logout.png'),
                                    Text('   ' + 'Log out', style: TextStyle(color: Colors.red, fontSize: 17.0)),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                  default:
                    return null;
                }
              },
              childCount: 3,
            ),
          ),
        ],
      ),
    );
  }
}
