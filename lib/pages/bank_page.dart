import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/asset_button.dart';
import 'package:trill/global.dart' as global;
import 'package:trill/kyc_alert.dart';
import 'package:trill/main_screen.dart';

class BankPage extends StatefulWidget {
  @override
  _BankPageState createState() => _BankPageState();
}

class _BankPageState extends State<BankPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        KYCAlert(),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left:18.0),
            child: Text(
              AppString.string(AppStringEnum.bank),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(context, FinancialAsset('US Dollar', type:FinancialAssetType.bank, image:Image.asset(
          "assets/usd.png",
          color: Colors.black,
        ))),
        buildAssetButton(context, FinancialAsset('Euro', type:FinancialAssetType.bank, image: Image.asset(
          "assets/eur.png",
          color: Colors.black,
        ))),
        buildAssetButton(context, FinancialAsset('Pound sterling', type:FinancialAssetType.bank, image: Image.asset(
          "assets/gbp.png",
          color: Colors.black,
        ))),
      ],
    );
  }
}
