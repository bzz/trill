import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:trill/main_screen.dart';
import 'package:trill/pages/notifications_page.dart';
import 'package:trill/pincode_screen.dart';
import 'package:trill/profile_screen.dart';
import 'app_localizations.dart';
import 'app_theme.dart';
import 'login_phone_code_screen.dart';
import 'login_phone_screen.dart';
import 'asset_screen.dart';
import 'global.dart' as global;
import 'dart:ui' as ui;

import 'notifications_screen.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await global.prefs.init();

//  var sns = await global.launchSNSMobileSDK();
//  print("SNS Completed with result: $sns");

//  global.canCheckBiometrics = await LocalAuthentication().canCheckBiometrics;

  runApp(
    MyApp(),
  );
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.white,
      statusBarBrightness: Brightness.dark,
      statusBarIconBrightness: Brightness.dark,
    ));
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: AppTheme.define(),
      home: Scaffold(
        body: SafeArea(child: global.prefs.loggedIn ? PincodeScreen() : LoginPhoneScreen()),
      ),
      routes: {
        '/pincode': (_) => PincodeScreen(),
        '/login': (_) => LoginPhoneScreen(),
        '/login_code': (_) => LoginPhoneCodeScreen(),
        '/main': (_) => MainScreen(),
        '/asset': (_) => AssetScreen(),
        '/notifications': (_) => NotificationsScreen(),
        '/profile': (_) => ProfileScreen(),
      },
      localizationsDelegates: [
        const AppLocalizationsDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: AppLocalizationsDelegate.supportedLanguages.map((lang) => Locale(lang, '')),
    );
  }
}
