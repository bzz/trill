import 'dart:io';
import 'package:flutter/material.dart';
import 'package:trill/global.dart' as global;
import 'app_localizations.dart';
import 'app_theme.dart';

class NotificationsScreen extends StatefulWidget {
  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  TabController _controller;
  int _index = 0;

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    _controller = TabController(vsync: this, length: 2, initialIndex: _index);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var side = MediaQuery.of(context).size.width * 0.25;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            elevation: 0,
            pinned: true,
            snap: true,
            floating: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text(
                AppString.string(AppStringEnum.notifications),
              ),
            ),
          ),
          SliverAppBar(
            leading: Container(),
            toolbarHeight: side + 70,
            automaticallyImplyLeading: false,
            pinned: true,
            snap: false,
            floating: false,
            flexibleSpace: Column(
              children: [
                TabBar(
                  controller: _controller,
                  labelPadding: EdgeInsets.zero,
                  labelStyle: TextStyle(fontSize: 15),
                  unselectedLabelColor: Colors.black,
                  indicatorSize: TabBarIndicatorSize.label,
                  indicator: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.red),
                  onTap: (index) {
                    setState(() {
                      _index = index;
                    });
                  },
                  tabs: [
                    Tab(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                          //   border: Border.all()
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(AppString.string(AppStringEnum.all_messages), style: TextStyle(fontSize: 13)),
                        ),
                      ),
                    ),
                    Tab(
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius:
                              BorderRadius.only(topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
                        ),
                        child: Align(
                          alignment: Alignment.center,
                          child: Text(AppString.string(AppStringEnum.unread_messages), style: TextStyle(fontSize: 13)),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Container(
                        height: 50,
                        child: Text('log ' + index.toString()),
                      ),
                    ),
                  ],
                );
              },
              childCount: 100,
            ),
          ),
        ],
      ),
    );
  }
}
