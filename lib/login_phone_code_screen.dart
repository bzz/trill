import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trill/app_localizations.dart';
import 'main_screen.dart';
import 'pincode_screen.dart';
import 'global.dart' as global;
import 'web.dart' as web;

class LoginPhoneCodeScreen extends StatefulWidget {
  @override
  _LoginPhoneCodeScreenState createState() => _LoginPhoneCodeScreenState();
}

class _LoginPhoneCodeScreenState extends State<LoginPhoneCodeScreen> {
  bool isLoading = false;
  var indicator = CircularProgressIndicator(strokeWidth: 10.0);
  TextEditingController c1;
  TextEditingController c2;
  TextEditingController c3;
  TextEditingController c4;
  TextEditingController c5;
  TextEditingController c6;

  @override
  void initState() {
    super.initState();
    c1 = TextEditingController();
    c2 = TextEditingController();
    c3 = TextEditingController();
    c4 = TextEditingController();
    c5 = TextEditingController();
    c6 = TextEditingController();
  }

  @override
  void dispose() {
    c1.dispose();
    c2.dispose();
    c3.dispose();
    c4.dispose();
    c5.dispose();
    c6.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.only(top: 40),
              child: Text(
                AppString.string(AppStringEnum.enter_code),
                style: TextStyle(fontSize: 25),
              ),
            ),
            Expanded(
              child: Container(),
            ),
            Focus(
              onFocusChange: (hasFocus) async {
                if (!hasFocus) {
                  var pin = c1.text + c2.text + c3.text + c4.text + c5.text + c6.text;
                  print(pin);
                  if (pin.length == 6) {
                    setState(() {
                      isLoading = true;
                    });
                    await requestLogin();
                    setState(() {
                      isLoading = false;
                    });
                  } else {
                    setState(() {
                      c1.text = '';
                      c2.text = '';
                      c3.text = '';
                      c4.text = '';
                      c5.text = '';
                      c6.text = '';
                    });
                  }
                }
              },
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildTextBox(c1),
                  _buildTextBox(c2),
                  _buildTextBox(c3),
                  _buildTextBox(c4),
                  _buildTextBox(c5),
                  _buildTextBox(c6),
                ],
              ),
            ),
            isLoading
                ? Padding(padding: EdgeInsets.all(20), child: CircularProgressIndicator(strokeWidth: 10.0))
                : Container(), //Submit Button
            SizedBox(
              child: OutlinedButton(
                onPressed: () async {
                  setState(() {
                    isLoading = true;
                  });
                  await requestLogin();
                  setState(() {
                    isLoading = false;
                  });
                },
                style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.black,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                ),
                child: Container(
                  width: double.infinity,
                  height: 50.0,
                  child: Center(
                    child: Text(
                        AppString.string(AppStringEnum.send_me_login_code),
                      style: TextStyle(fontSize: 18, color: Colors.white),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildTextBox(TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: SizedBox(
        height: 60,
        width: 40,
        // onFocusChange: (hasFocus) {
        //   setState(() {
        //     if (hasFocus)
        //     controller.text = '';
        //   });
        // },
        child: TextFormField(
          inputFormatters: [LengthLimitingTextInputFormatter(1), FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))],
          decoration: InputDecoration(
            border: OutlineInputBorder(),
          ),
          showCursor: false,
          enableInteractiveSelection: false,
          autofocus: FocusScope.of(context).isFirstFocus ? true : false,
          controller: controller,
          // onTap: () {
          //   print('tap');
          //
          //   setState(() {
          //     controller.text = '';
          //   });
          // },
          onChanged: (text) {
            if (text.isEmpty) {
              print (!FocusScope.of(context).isFirstFocus);
              FocusScope.of(context).previousFocus();
            }
            if (text.isNotEmpty) {
              FocusScope.of(context).nextFocus();
              controller.text = text;
            }
          },
          keyboardType: TextInputType.number,
        ),
      ),
    );
  }

  requestLogin() async {
    await Future.delayed(Duration(seconds: 1));
    global.prefs.loggedIn = true;
    Navigator.pushNamed(context, '/pincode');
  }
}
