import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

@immutable
class AppColors {
  final menu = const Color(0xfff5f3fa);
  final grey = const Color(0xffd1d1d1);
  final lightGrey = const Color(0xffe5e5e5);
  final pink = const Color(0xfff9cccc);
  final reddish = const Color(0xffF1ADAD);
  final lightBlue = const Color(0xffEFF3F8);
  final darkBlue = const Color(0xFF111C44);
  final backgroundBlue = const Color(0xfff1f6fa);
  final darkGreyFont = const Color(0xff9299B4);
  const AppColors();
}

@immutable
class AppTheme {
  static const colors = AppColors();

  const AppTheme._();

  static ThemeData define() {
    return ThemeData(
      brightness: Brightness.light,
      pageTransitionsTheme: PageTransitionsTheme(builders: {
        TargetPlatform.android: CupertinoPageTransitionsBuilder(),
      }),
//        fontFamily: 'Courier',
//      primaryColor: Colors.white,
//       primaryColor: Colors.black,
      primaryColorLight: Colors.white,
      primaryColorDark: AppTheme.colors.darkBlue,
      primarySwatch: Colors.grey,
      canvasColor: Color(0xfff5f3fa),
      accentColor: Color(0xff8E8E93),
      accentIconTheme: IconThemeData(color: Colors.black),
      focusColor: Color(0xffe93f3c),
      inputDecorationTheme: InputDecorationTheme(
        labelStyle: TextStyle(
          color: Colors.black,
        ),
        prefixStyle: TextStyle(color: Colors.black),
        border: OutlineInputBorder(
          borderSide: BorderSide(
            color: Colors.red,
            width: 3,
          ),
          borderRadius: BorderRadius.circular(4),
        ),
      ),
      textTheme: TextTheme(
        bodyText2: TextStyle(color: Colors.black, fontSize: 17.0),
        overline: TextStyle(color: AppTheme.colors.darkGreyFont, fontSize: 14.0, fontWeight: FontWeight.w500),
      ),
      appBarTheme: AppBarTheme(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        foregroundColor: Colors.black,
        systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.white),
      ),
    );
  }
}
