import 'package:flutter/material.dart';
import 'package:trill/app_theme.dart';
import 'package:trill/global.dart' as global;

enum FinancialAssetType {
  undefined,
  bank,
  crypto,
  trade,
}

class FinancialAsset {
  String name;
  Image image = Image.asset(
    "assets/bank.png",
    color: Colors.black,
  );
  FinancialAssetType type = FinancialAssetType.undefined;

  FinancialAsset(this.name, {this.type, this.image});
}

Widget buildAssetButton(BuildContext context, FinancialAsset asset) {
  Image im1 = asset.image;
  Widget im = Container(
      width: 32,
      height: 32,
      padding: EdgeInsets.all(8),
      decoration: BoxDecoration(color: AppTheme.colors.lightGrey, borderRadius: BorderRadius.circular(16)),
      child: im1);

  return Padding(
    padding: const EdgeInsets.only(left: 12, bottom: 12),
    child: OutlinedButton(
      onPressed: () {
        Navigator.pushNamed(context, '/asset');
      },
      style: OutlinedButton.styleFrom(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        side: BorderSide(width: 1, color: AppTheme.colors.lightGrey),
      ),
      child: Container(
          width: double.infinity,
          height: 70.0,
          child: Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 10),
                child: im,
              ),
//              BoxDecoration(color: Colors.red[500], borderRadius: BorderRadius.circular(25)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Divider(),
                  Text(
                    asset.name,
                    style: TextStyle(fontSize: 14, color: Colors.black, fontWeight: FontWeight.w400),
                  ),
                  global.prefs.hiddenWealth
                      ? Text('***')
                      : Text(
                          '0',
                          style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                        ),
                  Divider(),
                ],
              ),
            ],
          )),
    ),
  );
}
