import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/asset_button.dart';
import 'package:trill/global.dart' as global;
import 'package:trill/main_screen.dart';

class NotificationsPage extends StatefulWidget {
  @override
  _NotificationsPageState createState() => _NotificationsPageState();
}

class _NotificationsPageState extends State<NotificationsPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left: 18.0),
            child: Text(
              AppString.string(AppStringEnum.notifications),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
//        buildAssetButton(context, FinancialAsset('CHAT BUTTONS')),
      ],
    );
  }
}
