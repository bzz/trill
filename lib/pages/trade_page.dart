import 'package:flutter/material.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/asset_button.dart';
import 'package:trill/global.dart' as global;
import 'package:trill/kyc_alert.dart';
import 'package:trill/main_screen.dart';

class TradePage extends StatefulWidget {
  @override
  _TradePageState createState() => _TradePageState();
}

class _TradePageState extends State<TradePage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        KYCAlert(),
        Container(
          width: double.infinity,
          height: 50.0,
          alignment: Alignment.centerLeft,
          child: Padding(
            padding: EdgeInsets.only(left:18.0),
            child: Text(
              AppString.string(AppStringEnum.trading),
              style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
            ),
          ),
        ),
        buildAssetButton(context, FinancialAsset('GAZP')),
        buildAssetButton(context, FinancialAsset('SBER')),
      ],
    );
  }
}

//   @override
//   Widget build(BuildContext context) {
//     return WebviewScaffold(
//       url: 'https://ru.tradingview.com/chart/?symbol=' + list[index],
//       withJavascript: true,
//       withZoom: true,
//     );
//   }
// }
