import 'dart:ui';
import 'app_theme.dart';

Color colorFromHex(int hex) {
  var c = Color(hex);
  return Color.fromARGB(1, c.blue, c.green, c.red);
}

