library trill.web;

import 'dart:convert' as convert;
import 'dart:convert';
import 'package:http/http.dart' as http;

final signIn = 'https://app.trillions.io/sign_in';

void sendReqistration() async {

 var url = Uri.https('httpbin.org', '/post');
//   var url = Uri.https('app.trillions.io', '/sign_in');

  Map data = {
    'user': {
      'login': 'client@client.com',
      'password': 'P@ssw0rd',
      'remember_me': false
    }
  };

  String body = json.encode(data);


  print(url);
  print(body);


  // Await the http get response, then decode the json-formatted response.
  // var response = await http.post(url);
  var response = await http.post(url, headers: {"Content-Type": "application/json;charset=utf-8"}, body: body);
  // var response = await http.post(
  //   url: 'https://example.com',
  //   headers: {"Content-Type": "application/json"},
  //   body: body,
  //   // encoding: convert.Encoding(),
  // );

  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    print('RESPONSE: $jsonResponse');
    // var itemCount = jsonResponse['totalItems'];
    // print('Number of books about http: $itemCount.');
  } else {
    print('Request failed with status: ${response.statusCode}.');
    // var jsonResponse = convert.jsonDecode(response.body);
    // print('RESPONSE: $jsonResponse');
  }
}
