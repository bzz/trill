import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/app_theme.dart';
import 'main_screen.dart';
import 'pincode_screen.dart';
import 'global.dart' as global;
import 'web.dart' as web;

class LoginPhoneScreen extends StatefulWidget {
  @override
  _LoginPhoneScreenState createState() => _LoginPhoneScreenState();
}

class _LoginPhoneScreenState extends State<LoginPhoneScreen> {
  bool isLoading = false;
  var indicator = CircularProgressIndicator(strokeWidth: 10);
  TextEditingController _controller;
  bool textInputDelayed = false;
  PhoneNumber _phoneNumber;

  @override
  void initState() {
    super.initState();
    _controller = new TextEditingController(text: '79264045392');
    onTextChanged(_controller.text);
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
          child: Container(
            // color: Colors.white,
            child: Column(children: [
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  AppString.string(AppStringEnum.enter_phone_number),
                  style: TextStyle(fontSize: 26, fontWeight: FontWeight.w600),
                ),
              ),
              Expanded(child: Container()),
              Row(children: [
                Container(
                    alignment: Alignment.center,
                    width: 70,
                    child: Text(_phoneNumber == null ? '' : _phoneNumber.isoCode)),
                Flexible(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      // inputFormatters: [
                      //   FilteringTextInputFormatter.allow(RegExp(r'[0-9]'))
                      // ],
                      autofocus: true,
                      controller: _controller,
                      onChanged: onTextChanged,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        filled: true,
                        fillColor: AppTheme.colors.lightBlue,
//                        hintStyle: TextStyle(color: Colors.red[800], fontSize: 15, fontWeight: FontWeight.w400),
                        hintText: AppString.string(AppStringEnum.enter_phone_number),
                        labelText: AppString.string(AppStringEnum.phone_number),
                        prefixText: '+',
                        enabledBorder: InputBorder.none,
                        disabledBorder: InputBorder.none,
                        border: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
                        focusedBorder: OutlineInputBorder(borderSide: BorderSide.none, borderRadius: BorderRadius.circular(10)),
                      ),
                      onEditingComplete: _phoneNumber == null
                          ? () {
//                              Focus.of(context).requestFocus();
                            }
                          : () async {
                              setState(() {
                                isLoading = true;
                              });
                              await requestLogin();
                              setState(() {
                                isLoading = false;
                              });
                            },
                      style: TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                    ),
                  ),
                ),
              ]),
              OutlinedButton(
                onPressed: _phoneNumber == null
                    ? null
                    : () async {
                        setState(() {
                          isLoading = true;
                        });
                        await requestLogin();
                        setState(() {
                          isLoading = false;
                        });
                      },
                style: OutlinedButton.styleFrom(
                  backgroundColor: Colors.black,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20),
                  ),
                ),
                child: Container(
                  width: double.infinity,
                  height: 50.0,
                  child: Center(
                    child: Text(
                      AppString.string(AppStringEnum.continue_),
                      style: TextStyle(fontSize: 18, color: _phoneNumber == null ? Colors.grey : Colors.white),
                    ),
                  ),
                ),
              ),
              isLoading
                  ? Padding(padding: EdgeInsets.all(20), child: CircularProgressIndicator(strokeWidth: 10))
                  : Container(),
            ]),
          ),
        ));
  }

  onTextChanged(value) async {
    if (textInputDelayed) return;
    textInputDelayed = true;
    await Future.delayed(Duration(milliseconds: 400));
    textInputDelayed = false;

    setState(() {
      _controller.text = value.replaceAll('+', '');
      _controller.selection = TextSelection.fromPosition(TextPosition(offset: _controller.text.length));
    });
    _phoneNumber = await PhoneNumber.getRegionInfoFromPhoneNumber('+' + value);
    var numberType = await PhoneNumber.getPhoneNumberType(value, _phoneNumber.isoCode);
    setState(() {
      if (numberType != PhoneNumberType.MOBILE) {
        _phoneNumber = null;
      }
    });
  }

  requestLogin() {
    global.prefs.phone = _phoneNumber.phoneNumber;
    print(global.prefs.phone);
    Navigator.pushNamed(context, '/login_code');
  }
}
