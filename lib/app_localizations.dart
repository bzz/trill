import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart' show SynchronousFuture;
import 'global.dart' as global;

class AppLocalizationsDelegate extends LocalizationsDelegate<AppLocalizations> {
  static Iterable<String> supportedLanguages = AppString.arr.keys;

  const AppLocalizationsDelegate();

  @override
  bool isSupported(Locale locale) {
    return supportedLanguages.contains(locale.languageCode);
  }

  @override
  Future<AppLocalizations> load(Locale locale) {
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(LocalizationsDelegate<AppLocalizations> old) => true;
}

class AppLocalizations {
  AppLocalizations(this.locale);

  Locale locale;

  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }
}

class AppString {
  static String string(AppStringEnum str) {
    var arr = AppString.arr[global.locale.languageCode];
    var arr_en = AppString.arr['en'];
    if (arr.length > str.index) return arr[str.index];
    if (arr_en.length > str.index) return arr_en[str.index];
    return '';
  }

  static const Map<String, List<String>> arr = {
    "en": [
      "Enter phone number",
      "Phone number",
      "Continue",
      "Enter code",
      "Send me login code",
      "Home",
      "Bank",
      "Crypto",
      "Trading",
      "Chat",
      "Total balance",
      "Balance of Bank",
      "Balance of Crypto",
      "Trading assets",
      "Support",
      "Notifications",
      "Setup pass code",
      "Enter pass code",
      "Repeat pass code",
      "Bonus",
      "Refer-a-friend",
      "Verification",
      "Security",
      "Logout",
      "Profile",
      "All messages",
      "Unread",
    ],
    "ru": [
      "Введите номер телефона",
      "Номер телефона",
      "Продолжить",
      "Введите код",
      "Вышлите мне код",
      "Домой",
      "Банк",
      "Крипто",
      "Трейдинг",
      "Чат",
      "Общий баланс",
      "Баланс банка",
      "Баланс крипто",
      "Торговые ассеты",
      "Поддержка",
      "Уведомления",
      "Придумайте pass code",
      "Введите pass code",
      "Повторно введите pass code",
      "Бонус",
      "Пригласи друга",
      "Верификация",
      "Безопасность",
      "Выход",
      "Профиль",
      "Все сообщения",
      "Непрочтенные",
    ],
  };
}

enum AppStringEnum {
  enter_phone_number,
  phone_number,
  continue_,
  enter_code,
  send_me_login_code,
  home,
  bank,
  crypto,
  trading,
  chat,
  total_balance,
  balance_of_bank,
  balance_of_crypto,
  trading_assets,
  support,
  notifications,
  setup_pass_code,
  enter_pass_code,
  repeat_pass_code,
  bonus,
  refer_a_friend,
  verification,
  security,
  logout,
  profile,
  all_messages,
  unread_messages,
}
