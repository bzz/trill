library trill.global;

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_idensic_mobile_sdk_plugin/flutter_idensic_mobile_sdk_plugin.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:io';
import 'app_localizations.dart';
import 'common.dart';
import 'dart:ui' as ui;

bool canCheckBiometrics = false;

final prefs = SharedPrefs();

class SharedPrefs {
  static SharedPreferences instance;

  init() async {
    if (instance == null) {
      instance = await SharedPreferences.getInstance();
    }
  }

  bool get loggedIn => (instance.getBool('loggedIn')) ?? false;
  set loggedIn(bool value) {
    instance.setBool('loggedIn', value);
  }

  int get pincode => (instance.getInt('pincode')) ?? -1;
  set pincode(int value) {
    instance.setInt('pincode', value);
  }

  String get phone => (instance.getString('phone'));
  set phone(String value) {
    instance.setString('phone', value);
  }

  bool get hiddenWealth => (instance.getBool('hiddenWealth')) ?? false;
  set hiddenWealth(bool value) {
    instance.setBool('hiddenWealth', value);
  }
}

Locale locale = AppLocalizationsDelegate().isSupported(ui.window.locale) ? ui.window.locale : Locale('en','');

Future<SNSMobileSDKResult> launchSNSMobileSDK() async {
  final String apiUrl = "https://test-api.sumsub.com"; // https://api.sumsub.com
  final String flowName = "msdk-basic-kyc"; // or set up your own with the dashboard
  final String accessToken = "your_access_token"; // generate one on the backend

  final onTokenExpiration = () async {
    // call your backend to fetch a new access token (this is just an example)
    return Future<String>.delayed(Duration(seconds: 2), () => "new_access_token");
  };

  final SNSStatusChangedHandler onStatusChanged = (SNSMobileSDKStatus newStatus, SNSMobileSDKStatus prevStatus) {
    print("The SDK status was changed: $prevStatus -> $newStatus");
  };

  final snsMobileSDK = SNSMobileSDK.builder(apiUrl, flowName)
      .withAccessToken(accessToken, onTokenExpiration)
      .withHandlers(onStatusChanged: onStatusChanged)
      .withDebug(true)
      .withSupportEmail("support@your-company.com")
//      .withLocale(Locale("en")) // Optional, for cases when you need to override system locale
      .build();

  final SNSMobileSDKResult result = await snsMobileSDK.launch();

  return result;
}
