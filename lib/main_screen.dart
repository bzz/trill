import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trill/pages/home_page.dart';
import 'package:trill/pages/support_page.dart';
import 'app_theme.dart';
import 'pages/bank_page.dart';
import 'pages/crypto_page.dart';
import 'pages/notifications_page.dart';
import 'pages/trade_page.dart';
import 'pincode_screen.dart';
import 'global.dart' as global;
import 'app_localizations.dart';
import 'dart:ui' as ui;

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  TabController _controller;
  int _index = 0;
  int _chatTabIndex = 0;

  List<Widget> _widgetOptions = [
    HomePage(),
    BankPage(),
    CryptoPage(),
    TradePage(),
  ];

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    _controller = TabController(vsync: this, length: 2, initialIndex: _index);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      Navigator.pushNamedAndRemoveUntil(context, '/pincode', (_) => false);
    }
  }

  @override
  void didChangeLocales(List<Locale> locale) {
    super.didChangeLocales(locale);
    setState(() {
      global.locale = AppLocalizationsDelegate().isSupported(ui.window.locale) ? ui.window.locale : Locale('en', '');
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Scaffold(
        // endDrawer: MainDrawer(onTap: (context, i) {
        //   Navigator.pop(context);
        // }),
        backgroundColor: Colors.white,
        body: SafeArea(
          child: _buildFlexibleSpaceBar(context, _index),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        // selectedIconTheme: IconThemeData(color: Colors.black),
        unselectedFontSize: 11,
        selectedFontSize: 11,
        type: BottomNavigationBarType.fixed,
        currentIndex: _index,
        iconSize: 20,
        items: [
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/home.png'),
              size: 24,
              color: Colors.grey,
            ),
            activeIcon: ImageIcon(
              AssetImage('assets/home.png'),
              size: 24,
              color: Colors.black,
            ),
            label: AppString.string(AppStringEnum.home),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/bank.png'),
              size: 24,
              color: Colors.grey,
            ),
            activeIcon: ImageIcon(
              AssetImage('assets/bank.png'),
              size: 24,
              color: Colors.black,
            ),
            label: AppString.string(AppStringEnum.bank),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/crypto.png'),
              size: 24,
              color: Colors.grey,
            ),
            activeIcon: ImageIcon(
              AssetImage('assets/crypto.png'),
              size: 24,
              color: Colors.black,
            ),
            label: AppString.string(AppStringEnum.crypto),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/trading.png'),
              size: 24,
              color: Colors.grey,
            ),
            activeIcon: ImageIcon(
              AssetImage('assets/trading.png'),
              size: 24,
              color: Colors.black,
            ),
            label: AppString.string(AppStringEnum.trading),
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(
              AssetImage('assets/chat.png'),
              size: 24,
              color: Colors.grey,
            ),
            activeIcon: ImageIcon(
              AssetImage('assets/chat.png'),
              size: 24,
              color: Colors.black,
            ),
            label: AppString.string(AppStringEnum.support),
          ),
        ],
        onTap: (index) {
          setState(() {
            _index = index;
          });
        },
      ),
    );
  }

  Widget _buildFlexibleSpaceBar(BuildContext context, int index) {
    var _appBarTitle;
    switch (index) {
      case 0:
        _appBarTitle = GestureDetector(
          onTap: () {
            global.prefs.hiddenWealth = !global.prefs.hiddenWealth;

            /// DIRTY HACK TO REFRESH PAGE AND SHOW/HIDE WEALTH
            Navigator.pushReplacement(
              context,
              PageRouteBuilder(
                pageBuilder: (context, animation1, animation2) => MainScreen(),
                transitionDuration: Duration(seconds: 0),
              ),
            );
          },
          child: Padding(
            padding: EdgeInsets.only(left: Platform.isIOS ? 80 : 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Container(),
                ),
                Row(
                  children: [
                    Text(
                      AppString.string(AppStringEnum.total_balance) + ' ',
                      style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
                    ),
                    Icon(
                      Icons.remove_red_eye,
                      color: Colors.black,
                      size: 17,
                    ),
                  ],
                ),
                global.prefs.hiddenWealth
                    ? Text('***')
                    : Text(
                        '0 \$',
                        style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                      ),
              ],
            ),
          ),
        );
        break;
      case 1:
        _appBarTitle = Padding(
          padding: EdgeInsets.only(left: Platform.isIOS ? 80 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(),
              ),
              global.prefs.hiddenWealth
                  ? Text('***')
                  : Text(
                      '0 \$',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
              Text(
                AppString.string(AppStringEnum.balance_of_bank),
                style: TextStyle(fontSize: 14, color: AppTheme.colors.grey, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        );
        break;
      case 2:
        _appBarTitle = Padding(
          padding: EdgeInsets.only(left: Platform.isIOS ? 80 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(),
              ),
              global.prefs.hiddenWealth
                  ? Text('***')
                  : Text(
                      '0 \$',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
              Text(
                AppString.string(AppStringEnum.balance_of_crypto),
                style: TextStyle(fontSize: 14, color: AppTheme.colors.grey, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        );
        break;
      case 3:
        _appBarTitle = Padding(
          padding: EdgeInsets.only(left: Platform.isIOS ? 80 : 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Container(),
              ),
              global.prefs.hiddenWealth
                  ? Text('***')
                  : Text(
                      '0 \$',
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
                    ),
              Text(
                AppString.string(AppStringEnum.trading_assets),
                style: TextStyle(fontSize: 14, color: AppTheme.colors.grey, fontWeight: FontWeight.w400),
              ),
            ],
          ),
        );
        break;
      case 4:
        break;
    }
    switch (index) {
      case 0:
      case 1:
      case 2:
      case 3:
        return CustomScrollView(
          slivers: [
            SliverAppBar(
              //             elevation: 0,
              // bottom: PreferredSize(
              //   preferredSize: Size.fromHeight(100.0),
              //   child: Text('sss'),
              // ),
              actions: [
                Builder(
                  builder: (context) => Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: IconButton(
                      icon:
                      Padding(
                        padding: EdgeInsets.all(7),
                        child:
                        ImageIcon(
                          AssetImage('assets/bell.png'),
                          size: 26,
                          color: AppTheme.colors.darkBlue,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/notifications');
                      },
                      tooltip: AppString.string(AppStringEnum.notifications),
                    ),
                  ),
                ),
                Expanded(child:Container()),
                Builder(
                  builder: (context) => Padding(
                    padding: EdgeInsets.only(right: 10),
                    child: IconButton(
                      icon:
                      Padding(
                        padding: EdgeInsets.all(7),
                        child:
                        ImageIcon(
                          AssetImage('assets/person.png'),
                          size: 26,
                          color: AppTheme.colors.darkBlue,
                        ),
                      ),
                      onPressed: () {
                        Navigator.pushNamed(context, '/profile');
                      },
                      tooltip: AppString.string(AppStringEnum.profile),
                    ),
                  ),
                ),
              ],
              automaticallyImplyLeading: false,
              pinned: true,
              snap: true,
              floating: true,
              expandedHeight: 140,
              flexibleSpace: FlexibleSpaceBar(
                title: _appBarTitle,
                background: _index != 0
                    ? null
                    : Row(
                        children: [
                          Expanded(child: Container()),
                          TextButton(
                            onPressed: () {
                              print('sssss');
                            },
                            child: Padding(
                              padding: EdgeInsets.only(right: 40),
                              child: Container(
                                padding: EdgeInsets.all(8),
                                decoration: BoxDecoration(color: Colors.black, borderRadius: BorderRadius.circular(16)),
                                child: Row(
                                  children: [
                                    Padding(
                                      padding: EdgeInsets.only(right: 4),
                                      child: Container(
                                        width: 14,
                                        height: 14,
                                        child: Image.asset(
                                          "assets/bonus.png",
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                    Text(
                                      '0',
                                      style: TextStyle(fontSize: 17, fontWeight: FontWeight.w500, color: Colors.white),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
              ),
            ),
            // SliverToBoxAdapter(
            //   child: SizedBox(
            //     height: 20,
            //     child: Center(
            //       child: Text('Scroll to see the SliverAppBar in effect.'),
            //     ),
            //   ),
            // ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return _widgetOptions.elementAt(_index);
                },
                childCount: 1,
              ),
            ),
          ],
        );
        break;
      case 4:
        return CustomScrollView(
          slivers: [
            SliverAppBar(
              // bottom: PreferredSize(
              //   preferredSize: Size.fromHeight(100.0),
              //   child: Text('sss'),
              // ),
              actions: [
                Builder(
                  builder: (context) => IconButton(
                    icon: Container(
                      width: 32,
                      height: 32,
//                      padding: EdgeInsets.all(8),
                      // decoration: BoxDecoration(color: AppTheme.colors.grey, borderRadius: BorderRadius.circular(16)),
                      child: Image.asset(
                        "assets/person.png",
                        color: Colors.white,
                      ),
                    ),
                    onPressed: () {
                      Scaffold.of(context).openEndDrawer();
                    },
                    tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                  ),
                ),
              ],
              leading: Container(
                child: Padding(
                    padding: EdgeInsets.only(left: 20), child: Image.asset('assets/logo.png', fit: BoxFit.fitWidth)),
              ),
              automaticallyImplyLeading: false,
              pinned: true,
              snap: false,
              floating: true,
              flexibleSpace: FlexibleSpaceBar(
                title: Padding(
                  padding: EdgeInsets.only(right: 50),
                  child: Container(
                    decoration: BoxDecoration(borderRadius: BorderRadius.circular(10), color: AppTheme.colors.grey),
                    child: Padding(
                      padding: EdgeInsets.all(2),
                      child: TabBar(
                        controller: _controller,
                        labelPadding: EdgeInsets.zero,
                        labelStyle: TextStyle(fontSize: 15),
                        unselectedLabelColor: Colors.black,
                        indicatorSize: TabBarIndicatorSize.label,
                        indicator: BoxDecoration(borderRadius: BorderRadius.circular(10), color: Colors.white),
                        onTap: (index) {
                          setState(() {
                            _chatTabIndex = index;
                          });
                        },
                        tabs: [
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.only(topLeft: Radius.circular(10), bottomLeft: Radius.circular(10)),
                                //   border: Border.all()
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child: Text(AppString.string(AppStringEnum.support), style: TextStyle(fontSize: 13)),
                              ),
                            ),
                          ),
                          Tab(
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius:
                                    BorderRadius.only(topRight: Radius.circular(10), bottomRight: Radius.circular(10)),
                              ),
                              child: Align(
                                alignment: Alignment.center,
                                child:
                                    Text(AppString.string(AppStringEnum.notifications), style: TextStyle(fontSize: 13)),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                background: Row(
                  children: [
                    Expanded(child: Container()),
                    Text(
                      'T0',
                      style: TextStyle(fontSize: 25),
                    ),
                  ],
                ),
              ),
            ),
            // SliverToBoxAdapter(
            //   child: SizedBox(
            //     height: 20,
            //     child: Center(
            //       child: Text('Scroll to see the SliverAppBar in effect.'),
            //     ),
            //   ),
            // ),
            SliverList(
              delegate: SliverChildBuilderDelegate(
                (BuildContext context, int index) {
                  return _chatTabIndex == 0 ? SupportPage() : NotificationsPage();
                },
                childCount: 1,
              ),
            ),
          ],
        );
        break;
    }
  }
}
