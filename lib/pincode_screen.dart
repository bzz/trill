import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:trill/app_localizations.dart';
import 'package:trill/app_theme.dart';
import 'package:trill/global.dart' as global;
import 'main_screen.dart';
import 'dart:math' as math;

class PincodeScreen extends StatefulWidget {
  @override
  _PincodeScreenState createState() => _PincodeScreenState();
}

enum PinInputStatus { setup, repeat, normal }

class _PincodeScreenState extends State<PincodeScreen> {
  int _firstPin = -1;
  List<int> _pin = [];
  Color _pinCircleColor = Colors.grey;

  var _pinInputStatus;

  // @override
  // void initState() {
  //   super.initState();
  //   stderr.writeln(global.prefs.pincode.toString());
  //   // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //   //   content: Text(global.prefs.pincode.toString()),
  //   // ));
  // }

  int _pin2int() {
    int out = 0;
    for (int i = 0; i < _pin.length; ++i) {
      out += _pin[i] * math.pow(10, 3 - i);
    }
    return out;
  }

  @override
  Widget build(BuildContext context) {
    _pinInputStatus = global.prefs.pincode >= 0
        ? PinInputStatus.normal
        : _firstPin < 0
            ? PinInputStatus.setup
            : PinInputStatus.repeat;
    return Scaffold(
        backgroundColor: Colors.white,
        body: SafeArea(
            child: Container(
          // decoration: BoxDecoration(color: Colors.blueGrey),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.only(top: 40),
                child: Text(
                  _pinInputStatus == PinInputStatus.setup
                      ? AppString.string(AppStringEnum.setup_pass_code)
                      : _pinInputStatus == PinInputStatus.repeat
                          ? AppString.string(AppStringEnum.repeat_pass_code)
                          : AppString.string(AppStringEnum.enter_pass_code),
                  style: TextStyle(fontSize: 25),
                ),
              ),
              Spacer(),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildPinCircle(context, 0),
                  _buildPinCircle(context, 1),
                  _buildPinCircle(context, 2),
                  _buildPinCircle(context, 3),
                ],
              ),
              Expanded(child: Container()),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildPinButton(context, 1),
                  _buildPinButton(context, 2),
                  _buildPinButton(context, 3),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildPinButton(context, 4),
                  _buildPinButton(context, 5),
                  _buildPinButton(context, 6),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildPinButton(context, 7),
                  _buildPinButton(context, 8),
                  _buildPinButton(context, 9),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  _buildPinButton(context, -1, () {
                    global.prefs.pincode = -1;
                    Navigator.pushNamedAndRemoveUntil(context, '/login', (_) => false);
                  }),
                  _buildPinButton(context, 0),
                  _buildPinButton(context, -2, () {
                    setState(() {
                      if (_pin.length > 0) _pin.removeLast();
                    });
                  }),
                ],
              ),
            ],
          ),
        )));
  }

  Widget _buildPinButton(BuildContext context, int number, [fn]) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: number < 0
          ? GestureDetector(
              onTap: () {
                HapticFeedback.vibrate();
                fn();
              },
              child: Container(
                decoration: BoxDecoration(color: Colors.transparent, borderRadius: BorderRadius.circular(30)),
                width: MediaQuery.of(context).size.width * 0.3,
                height: 70,
                child: Center(
                  child: number == -2
                      ? Icon(Icons.backspace)
                      : Text(
                          'Exit',
                          style: TextStyle(fontSize: 25),
                        ),
                ),
              ))
          : GestureDetector(
              onTap: () {
                HapticFeedback.vibrate();
                if (_pin.length > 4) return;
                setState(() {
                  _pin.add(number);
                  _pinCircleColor = Colors.grey;
                });
                if (_pin.length < 4) return;
                switch (_pinInputStatus) {
                  case PinInputStatus.setup:
                    _firstPin = _pin2int();
                    setState(() {
                      _pinInputStatus = PinInputStatus.repeat;
                      _pin = [];
                    });
                    break;
                  case PinInputStatus.repeat:
                    if (_pin2int() == _firstPin) {
                      global.prefs.pincode = _firstPin;
                      setState(() {
                        _pinInputStatus = PinInputStatus.normal;
                      });
                      _pin = [];
                      Navigator.push(context, MaterialPageRoute(builder: (_) => MainScreen()));
                    } else {
                      setState(() {
                        _pin = [];
                        _pinCircleColor = Colors.red;
                      });
                    }
                    setState(() {
                      _pinInputStatus = PinInputStatus.setup;
                    });
                    _firstPin = -1;
                    break;
                  case PinInputStatus.normal:
                    if (_pin2int() == global.prefs.pincode) {
                      setState(() {
                        _pin = [];
                      });
                      Navigator.push(context, MaterialPageRoute(builder: (_) => MainScreen()));
                    } else {
                      setState(() {
                        _pin = [];
                        _pinCircleColor = Colors.red;
                      });
                    }
                    break;
                }

                // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                //   content: Text(_pin2int().toString()),
                // ));
              },
              child: Container(
                decoration: BoxDecoration(color: Colors.transparent, borderRadius: BorderRadius.circular(30)),
                width: MediaQuery.of(context).size.width * 0.3,
                height: 70,
                child: Center(
                  child: Text(
                    number.toString(),
                    style: TextStyle(fontSize: 25),
                  ),
                ),
              )),
    );
  }

  Widget _buildPinCircle(BuildContext context, value) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Container(
        width: 10,
        height: 10,
        decoration: BoxDecoration(
            color: _pin.length > value ? Colors.black : _pinCircleColor, borderRadius: BorderRadius.circular(5)),
      ),
    );
  }
}
