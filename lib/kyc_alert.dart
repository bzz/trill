import 'package:flutter/material.dart';

import 'app_theme.dart';

class KYCAlert extends StatelessWidget {
  const KYCAlert({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Container(
        decoration: BoxDecoration(color: AppTheme.colors.pink, borderRadius: BorderRadius.circular(16)),
        width: double.infinity,
        height: 120.0,
        alignment: Alignment.centerLeft,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Container(
                width: 32,
                height: 32,
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(color: Colors.white, borderRadius: BorderRadius.circular(16)),
                child: Image.asset(
                  "assets/attention.png",
                  color: AppTheme.colors.reddish,
                ),
              ),
            ),
            Flexible(
              child: Padding(
                padding: const EdgeInsets.only(top: 18.0, left: 0, right: 60, bottom: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                      'Attention',
                      style: TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                    Expanded(child: Container()),
                    const Text(
                      'You may want to pass verification to proceed with most operations',
                      maxLines: 6,
                      style: TextStyle(fontSize: 14),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
