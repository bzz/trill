import 'dart:io';
import 'package:flutter/material.dart';
import 'package:trill/global.dart' as global;
import 'app_theme.dart';

class AssetScreen extends StatefulWidget {
  @override
  _AssetScreenState createState() => _AssetScreenState();
}

class _AssetScreenState extends State<AssetScreen> {
  bool didAuthenticate;

  @override
  Widget build(BuildContext context) {
    var _appBarTitle = Padding(
      padding: EdgeInsets.only(left: Platform.isIOS ? 80 : 0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(),
          ),
          Row(
            children: [
              Text(
                'US Dollar ',
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.w400),
              ),
            ],
          ),
          Text(
            '0 \$',
            style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500),
          ),
        ],
      ),
    );
    var side = MediaQuery.of(context).size.width * 0.25;
    return Scaffold(
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            elevation: 0,
            pinned: true,
            snap: true,
            floating: true,
            expandedHeight: 140,
            flexibleSpace: FlexibleSpaceBar(
              title: _appBarTitle,
            ),
          ),
          SliverAppBar(
            leading: Container(),
            toolbarHeight: side + 70,
            automaticallyImplyLeading: false,
            pinned: true,
            snap: false,
            floating: false,
            flexibleSpace: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Container(
                        width: side,
                        height: side,
                        child: OutlinedButton(
                          onPressed: () {},
                          style: OutlinedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/receive.png',
                                width: 26,
                                color: AppTheme.colors.grey,
                              ),
                              Divider(color: Colors.transparent),
                              Text(
                                'Receive',
                                style: TextStyle(fontSize: 15, color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Container(
                        width: side,
                        height: side,
                        child: OutlinedButton(
                          onPressed: () {},
                          style: OutlinedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/send.png',
                                width: 26,
                                color: AppTheme.colors.grey,
                              ),
                              Divider(color: Colors.transparent),
                              Text(
                                'Send',
                                style: TextStyle(fontSize: 15, color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Container(
                        width: side,
                        height: side,
                        child: OutlinedButton(
                          onPressed: () {},
                          style: OutlinedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12),
                            ),
                          ),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image.asset(
                                'assets/exchange.png',
                                width: 26,
                                color: AppTheme.colors.grey,
                              ),
                              Divider(color: Colors.transparent),
                              Text(
                                'Exchange',
                                style: TextStyle(fontSize: 15, color: Colors.black),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Divider(color: Colors.transparent),
                Container(
                  width: double.infinity,
                  height: 50.0,
                  alignment: Alignment.centerLeft,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 18.0),
                    child: const Text(
                      'Logs',
                      style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                return Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.all(8),
                      child: Container(
                        height: 50,
                        child: Text('log ' + index.toString()),
                      ),
                    ),
                  ],
                );
              },
              childCount: 100,
            ),
          ),
        ],
      ),
    );
  }
}
